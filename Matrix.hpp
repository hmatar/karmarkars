// This class abstracts a matrix.
// It holds the contents of a matrix and dimensions.
#ifndef MATRIX_KARMARKA
#define MATRIX_KARMARKA

#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

class Matrix {
  public:
    long m;    // number of rows
    long n;    // number of columns
    double ** M; // contents of a matrix

    /**
     * Matrix constructor for empty matrix
     */
    Matrix(long _m, long _n){
         init(_m, _n);
    }

    /**
     * A common function used by constructors for initialization
     */
    void init(long _m, long _n) {

        m = _m;
        n = _n;

        // check if sizes are not negative
        if(_m <= 0 || _n <= 0) {
            cout << "Matrix size should be greater than 0" << endl;
            cout << "You used dimensions " << _m << ", " << _n << endl;
            cout << "Exiting ..." << endl;
            exit(0);
        }

        // create space for matrix
        M = new double*[_m];
        for(int row = 0; row < _m; ++row)
            M[row] = new double[_n];

        // initialise to zeros
        for(long Row = 0; Row < _m; ++Row)
            for(long Col = 0; Col < _n; ++Col)
                M[Row][Col] = 0;
    }


    /**
     * Matrix constructor using another matrix
     */
    Matrix(Matrix & _matrix) {

        // initialize
        init(_matrix.m, _matrix.n);

        // copy contents

        for(int row = 0; row < m; ++row)
            for(int col =0; col < n; ++col)
                M[row][col] = _matrix.M[row][col];
    }


    /**
     * returns true if matrix is square
     */
    bool isSquare() {
        return n == m;
    }

    /**
     * A destructor of the matrix
     */
    ~Matrix() {

        for(int row = 0; row < m; ++row)
            delete M[row];
        delete M;
    }


    /**
     * Override function for printing to a file or console.
     */
    friend ostream& operator<<(ostream& out, const Matrix& matrix) {

        for(int row = 0; row < matrix.m; ++row) {
            out << fixed;
            out << "[ "<< setprecision(6) << (abs(matrix.M[row][0]) < 0.0000001? 0: matrix.M[row][0]);
            for(int col =1; col < matrix.n; ++col) {
                out << fixed;
                out << ", "<< setw(8) << setprecision(6) << (abs(matrix.M[row][col]) < 0.0000001? 0 : matrix.M[row][col]);
            }
            out << " ]"<< endl;
        }

        return out;
    }
};

#endif // MATRIX_KARMARKA
