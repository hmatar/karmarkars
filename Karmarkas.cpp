// This is the main file of the Karmarka's method
#include <iostream>
#include <cmath>
#include "Matrix.hpp"
#include "MatrixManip.hpp"
#include "Iteration.hpp"

using namespace std;

double getLoweBound(Matrix& A, Matrix &C) {
  double L = 0;

  //find maximum objective coefficient
  double Cjmax = 0;
  for(int row = 0; row < C.m; row++)
     for(int col = 0; col < C.n; col++)
        if(abs(C.M[row][col]) > Cjmax)
            Cjmax = abs(C.M[row][col]);

  // logs of matrix A
  double sumLogs = 0;
  for(int row = 0; row < A.m; row++)
     for(int col = 0; col < A.n; col++)
       sumLogs += log(1 + abs(A.M[row][col]));

  L = 1 + log(1 + Cjmax) + log(1+A.m) + sumLogs;
  return L;
}
int main() {

  // Initial data
  long n = 5;
  long m = 2;
  /*
  // matrices
  Matrix A(1, 3);
  A.M[0][0] = 1;
  A.M[0][1] = -2;
  A.M[0][2] = 1;

  Matrix C(1, 3);
  C.M[0][0] = 0;
  C.M[0][1] = 2;
  C.M[0][2] = -1;

   Matrix A(1,3);
  A.M[0][0] = 1;
  A.M[0][1] = 1;
  A.M[0][2] = -2;

  Matrix C(1, 3);
  C.M[0][0] = 0;
  C.M[0][1] = 1;
  C.M[0][2] = 0;
  */

  Matrix A(2, 5);
  A.M[0][0] = 1;
  A.M[0][1] = -1;
  A.M[0][2] = 2;
  A.M[0][3] = 0;
  A.M[0][4] = -2;

  A.M[1][0] = 1;
  A.M[1][1] = 2;
  A.M[1][2] = 0;
  A.M[1][3] = 1;
  A.M[1][4] = -4;

  Matrix C(1,5);
  C.M[0][0] = -1;
  C.M[0][1] = -2;
  C.M[0][2] = 0;
  C.M[0][3] = 0;
  C.M[0][4] = 4;

  ///  Step 0: initialization  ///
  //   initial solution
  Matrix *X0 = Iteration::genX0(n);
  Matrix *Xk = X0;
  //   step length parameters
  double r = 1.0 / sqrt(n * ( n - 1));
  cout <<"r: "<< r << endl;

  double alpha = (n - 1.0) / (3.0 * n);
  cout <<"alpha: "<< alpha << endl;

  int k = 0; // iteration number
  cout << "k: " << k << endl;
  double Zk = 1.0;

  // Calculate Lower bound on the input length for iterations
  double L = getLoweBound(A, C);
  double epsilon = 0.000001;
  ///  Iterations New step
  while(Zk > pow(2, -L) * epsilon ) {
      cout << "=================================k=" << k <<endl;
      cout << "Dziii\n"<< *X0 << endl;
      Iteration Iter0(k, *Xk);

      // Define Dk
      Matrix *Dk  = Iter0.getDiag(*Xk);
      cout <<"DK: \n"<< *Dk << endl;

      // Calculate P
      Matrix *P = Iter0.getP(A, *Dk);
      cout << "P: \n" << *P;
      // Calculate cp
      Matrix *cp = Iter0.calculatedCP(C);

      // calculate Ynew
      Matrix* Ynew = Iter0.calculateYnew(r, alpha, *X0);

      cout << "C: \n" << C;
      Xk = Iter0.calculateXnew(*Ynew, *Dk);

      Matrix *Z = multiply(C, *Xk );
      cout << "Karmarka's Algorithm" << *Z << endl;
      Zk = (*Z).M[0][0];

      k++; // increment step number
  }

  cout << "\n==========================\n";
  cout << "Optimal solution: \n" << *Xk;
  cout << "\n";
  cout << fixed;
  cout << "Optimal value: " << Zk << endl;
  cout << "\n==========================\n";

  return 0;
}

