# README #

This page documents the implementation of original Karmarka's method.

### Prerequisites ###
This application has been developed entirely on a Linux machine but should work on Windows/Cygwin and Mac OS.

* Any C++ compiler: icc, gcc, clang.
* Linux Cmake

### How to build ###
```
#!Shell

$> make
```


### How run ###
```
#!Shell

$> karmarkas.exe
```


### Copyright notice ###
All codes and this README are copyrighted to Hassan Salehe Matar.
This code has been implemented to fullfil requirements for COMP501 - Optimization Models and Algorithms at Koc University

(c) Hassan Salehe Matar. Copying this project or part of without consent of the owner or the instructor of COMP501 is strictly prohibited.