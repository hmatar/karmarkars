// Contains a class for general functions for different matrix
// manipulation functions like transpose, inverse, multiplication,
// vector distance.

#ifndef MATRIX_MANIP_KARMARKA
#define MATRIX_MANIP_KARMARKA

#include <iostream>
#include "Matrix.hpp"

using namespace std;

/**
 * This function computes transporse of matrix
 * @return Returns a pointer to a transpose of the matrix.
 */
Matrix * transpose(Matrix & matrix) {

    // create empty matrix
    Matrix *transM = new Matrix(matrix.n, matrix.m);

    // copy transpose
    for(long origRow = 0; origRow < matrix.m; ++origRow)
        for(long origCol = 0; origCol < matrix.n; ++origCol)
            (transM->M)[origCol][origRow] = matrix.M[origRow][origCol];

    return transM;
}

/**
 * This function returns an identity matrix of size nxn
 * @param n one side dimension of identity matrix to be created
 * @return Returns a pointer to a transpose of the matrix.
 */
Matrix * identity(long n) {

    // create empty matrix
    Matrix *transM = new Matrix(n, n);

    // copy
    for(long i = 0; i < n; ++i)
        (transM->M)[i][i] = 1;

    return transM;
}

/**
 * Subtracts matrix B from A
*/
Matrix * subtract(Matrix & A, Matrix & B) {
   if(A.m != B.m || A.n != B.n) {
     cout << "Warning !! subtracting matrices of incompatible dimensions" << endl;
     return NULL;
   }

   Matrix * Res = new Matrix(A.m, A.n);

   for(int r = 0; r < A.m; ++r)
       for(int c = 0; c < A.n; ++c)
       (Res->M)[r][c] = A.M[r][c] - B.M[r][c];
   return Res;
}

/**
 * Multiplies two matrices.
 * @param A matrix A
 * @param B matrix B
 * @require A.n == B.m
 * @result a result of matrix multiplication.
 */
Matrix * multiply(Matrix & A, Matrix& B) {

    // check if compatible size
    if(A.n != B.m) {
        cout << "An: " << A.n << " B.m: " << B.m << endl;
        cout << "Warning!! multiply matrices of incompatible size";
        cout << "          Could not multiply" << endl;
        return NULL;
    }

    // create empty matrix to hold product.
    Matrix * multiM = new Matrix(A.m, B.n);

    //Multiply A & B and store in multiM
    for(long i=0; i<A.m; ++i)
        for(long j=0; j<B.n; ++j)
            for(long k=0; k<A.n; ++k)
                (multiM->M)[i][j]+= A.M[i][k] * B.M[k][j];

    return multiM;

}


/**
 * Calculates a diagonal matrix of a given matrix.
 * @param matrix a column vector of size mxn where n = 1
 */
Matrix* diag(Matrix & matrix) {
    if(matrix.n != 1) {
        cout << "Warning! generating diagonal matrix from a non-vector" << endl;
    }

    // generate identity matrix
    Matrix *D = identity(matrix.m);

    // get diagonal
    for(int dd = 0; dd < matrix.m; ++dd)
        (D->M)[dd][dd] = matrix.M[dd][0];

    return D;
}


/**
 * Computes determinant of a matrix using expansion by minors.
 * @param A matrix whose determinant to be found
 * @return determinant
 */
double Determinant(Matrix & A)
{
   int i,j,j1,j2;
   double detA = 0;
   double** a = A.M;

   // temp holder
   Matrix * TempM = NULL;

   long n = A.n;

   if (n < 1) {
      cout << "Warning !! can't find determinant of matrix with size less than 1"<< endl;
    }
    else if (n == 1) { // 1x1 matrix
      detA = a[0][0];
    }
    else if (n == 2) { // 2x2 matrix
        detA = a[0][0] * a[1][1] - a[1][0] * a[0][1];
    }
    else { // rest
      detA = 0;
      for (j1 = 0; j1 < n; j1++) {

         TempM = new Matrix(n-1, n-1);
         for (i = 1; i < n; i++) {
            j2 = 0;
            for (j=0;j<n;j++) {
               if (j == j1)
                  continue;
               (TempM->M)[i-1][j2] = a[i][j];
               j2++;
            }
         }
         detA += pow(-1.0,j1+2.0) * a[0][j1] * Determinant(*TempM);
         delete TempM;
      }
   }

   return detA;
}


/**
 * Computes the cofactor matrix of a square matrix
 * @param A original matrix
 * @return cofactor of matrix
 */
Matrix * CoFactor(Matrix& A)
{
    int ii, jj, i1, j1;

    double det = 0;
    Matrix * Cof = new Matrix(A.n, A.n);

    // temp matrix
    Matrix *C = new Matrix(A.n - 1, A.n - 1);

    for(int j = 0; j < A.n; j++) {
        for (int i = 0; i < A.n; i++) {

        // Form the adjoint at A[i,j]
        i1 = 0;
        for (ii = 0; ii < A.n; ii++) {
            if (ii == i)
               continue;
            j1 = 0;
            for (jj = 0; jj < A.n; jj++) {
               if (jj == j)
                  continue;
               (C->M)[i1][j1] = A.M[ii][jj];
               j1++;
            }
            i1++;
        }

        // Calculate the determinate
        det = Determinant(*C);

        // Fill in the elements of the cofactor
        (Cof->M)[i][j] = pow(-1.0, i + j + 2.0) * det;
      }
    }
    delete C;

    return Cof;
}

/**
 * Calculates inverse of a matrix
 * @return inverse of matrix
 */
Matrix * inverse(Matrix & A) {
    double detA = Determinant(A);
    cout << "det: " << detA << endl;
    Matrix *cofA = CoFactor(A);
    cout << "Cofactor\n" << *cofA;
    Matrix *adjA = transpose(*cofA);

    // divide by determinant
    for(int i = 0; i < adjA->n; ++i)
    {
       for(int j = 0; j < adjA->n; ++j)
        (adjA->M)[i][j] /= detA;
    }
    return adjA; // return inverse
}

#endif // MATRIX_MANIP_KARMARKA
