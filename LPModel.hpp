#ifndef LPMODEL_KARMARKA
#define LPMODEL_KARMARKA

// THis class represents the model of the problem

class LPModel{
  public:
    Matrix A; // the A matrix
    Matrix b; // r.h.s vector
    Matrix c; // objective coefficient matrix
    bool transformed = false;
    void init() {
    }

};

#endif //LPMODEL_KARMARKA
