// Holds all related data at a given iteration
#ifndef ITERATION_KARMARKA
#define ITERATION_KARMARKA


#include "MatrixManip.hpp"

class Iteration {
  public:
    int k;
    Matrix * Xk;
    Matrix * Xk_1;
    Matrix * Yk;
    Matrix * Dk;
    Matrix * P;
    Matrix * cp;
    Matrix * Ynew;
    double Z;
    Iteration(int _k, Matrix & _Yk): k(_k) {
      cout << "Dziii\n"<< _Yk << endl;
       Yk = new Matrix(_Yk);
       cout << "_Yk" << *Yk;
    }


    /**
     * Generate initial solution at initialization
     * @param n number of variables
     * @return X0, a column vector whose entries are 1/n each.
     */
    static Matrix * genX0(long n) {
       Matrix *X0 = new Matrix(n, 1);
       for(int row=0; row < n; ++row)
       {
          (X0->M)[row][0] = 1.0 / n;
       }
       return X0;
    }


    /**
     * Calculates diagonal of a matrix
     *
     */
    Matrix *getDiag(Matrix& X0) {
        Dk = diag(X0);
        return Dk;
    }

    /**
     * Computes P = ADk 1
     */
    Matrix * getP(Matrix& A, Matrix & Dk) {
        Matrix * ADk = multiply(A, Dk);

        // add one row with ones
        P = ADk;
        double** M = new double*[ADk->m + 1];
        double * ones = new double[ADk->n];
        for(int i = 0; i < ADk->n; ++i)
            ones[i] = 1;

        for(int i = 0; i < ADk->m; ++i)
        {
          M[i] = (ADk->M)[i];
          (ADk->M)[i] = NULL;
        }
        M[ADk->m] = ones;
        delete ADk->M;

        P->m = P->m +1;
        P->M = M;
        return P;
    }

    Matrix * calculatedCP(Matrix & C) {
       // Calculate CDk
       Matrix * CDk = multiply(C, *Dk);
       // CDk transpose
       Matrix* CDk_transpose = transpose(*CDk);

       cout << "CDk: \n" << *CDk;

       // calculate P-transpose
       Matrix * P_transp = transpose(*P);

       // calculate PPt
       Matrix * PPt = multiply(*P, *P_transp);
       cout << "PPt: \n" << *PPt;

       // calculate PPt-inverse
       Matrix * PPt_inv = inverse(*PPt);
       cout << "PPt_inv: \n" << *PPt_inv;

       // calculate PPt_inv * P
       Matrix * PPt_invP = multiply(*PPt_inv, *P);

       // calculate P-transp * PPt_invP
       Matrix * P_trans_PPt_invP = multiply(*P_transp, *PPt_invP);
       cout << "P-transPPt_invP: \n" << *P_trans_PPt_invP;

       // get identity matrix
       Matrix * I = identity(P_trans_PPt_invP->n);

       // subtruct P_trans_PPt_invP from I
       Matrix* subRes = subtract(*I, *P_trans_PPt_invP);

       cp = multiply(*subRes, *CDk_transpose);
       cout << "===========cp========\n" << *cp;
       delete CDk; delete CDk_transpose; delete P_transp;  delete PPt;
       delete PPt_inv; delete PPt_invP; delete P_trans_PPt_invP;
       delete I; delete subRes;

       return cp;
    }

    Matrix * calculateYnew(double r, double alpha, Matrix & Y0) {
      // calculate norm of cp
      double cp_norm = 0;
      for(int i=0; i < cp->m; ++i)
          cp_norm += ((cp->M)[i][0] * (cp->M)[i][0]);

      cp_norm = sqrt(cp_norm);
      cout << "cp norm: " << cp_norm << endl;

     // divide cp by norm  and multiply with r, alpha
      for(int i = 0; i < cp->m; ++i)
          (cp->M)[i][0] = (( alpha * r *(cp->M)[i][0] ) / cp_norm);

      cout << "YK: " << *Yk;
      Ynew = subtract(Y0, *cp);

      cout << "Ynew: \n" << *Ynew;
      return Ynew;
   }

   Matrix * calculateXnew(Matrix & Ynew, Matrix & Dk) {

      Matrix * DkYnew = multiply(Dk, Ynew);

      double _1DkYnew = 0;
      for(int i = 0; i < DkYnew->m; ++i)
         _1DkYnew += (DkYnew->M)[i][0];

      for(int i = 0; i < DkYnew->m; ++i)
          (DkYnew->M)[i][0] /= _1DkYnew;

      Xk_1 = DkYnew;
      cout << "Xk+1: \n" << *DkYnew;
      return Xk_1;
   }
};

#endif // ITERATION_KARMARKA
